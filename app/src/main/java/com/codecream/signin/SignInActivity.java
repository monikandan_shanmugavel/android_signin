package com.codecream.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, INetworkThreadListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        Button signInBtn = (Button) this.findViewById(R.id.signInBtn);
        signInBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText userNameField = (EditText) this.findViewById(R.id.userName);
        EditText passwordField = (EditText) this.findViewById(R.id.signInPassword);

        String userName = userNameField.getText().toString();
        String password = passwordField.getText().toString();
        password = md5(password);

        JSONObject params = new JSONObject();
        try {
            params.put("userName", userName);
            params.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = "https://codecream.herokuapp.com/loginUser";
        ProgressDlgHelper.show(this, "Signing In...");
        NetworkThread networkThread = new NetworkThread(NetworkThread.POST_REQUEST, url, params, this);
        networkThread.execute();

    }

    public String md5(String inputString) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(inputString.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onRequestComplete(String url, String resultStr) {
        ProgressDlgHelper.dismiss();

        try {
            JSONObject response = new JSONObject(resultStr);
            String status = response.optString("status", "");
            if(status.compareTo("success") == 0){
                Log.i("signin", "success");
                Intent signInIntent = new Intent();
                signInIntent.putExtra("signedIn", true);
                setResult(RESULT_OK, signInIntent);
                this.finish();
            }
            else{
                Log.i("signin", "failed");
                AlertDlgHelper.showAlert(this, "SignIn Failed", "User name or password does not match");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
