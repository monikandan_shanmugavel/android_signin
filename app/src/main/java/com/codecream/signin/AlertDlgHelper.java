package com.codecream.signin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AlertDlgHelper {
    public static void showAlert(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dlg = builder.create();
        dlg.show();
    }
}
