package com.codecream.signin;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;

public class NetworkThread extends AsyncTask<Void, Void, Boolean> {

    private String url;
    private INetworkThreadListener listener;
    private String apiResult;
    private int requestType;
    private JSONObject params;

    public final static int GET_REQUEST = 1;
    public final static int POST_REQUEST = 2;

    public NetworkThread(int requestType, String url, JSONObject params, INetworkThreadListener listener){
        this.requestType = requestType;
        this.url = url;
        this.params = params;
        this.listener = listener;
    }

    private Request buildRequest(){
        Request request = null;
        if(requestType == GET_REQUEST)
            request = new Request.Builder().url(url).build();
        else if(requestType == POST_REQUEST){
            MediaType typeJson = MediaType.parse("application/json; charset=utf-8");
            RequestBody body = RequestBody.create(typeJson, params.toString());
            request = new Request.Builder().url(url).post(body).build();
        }
        return request;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OkHttpClient client = new OkHttpClient();
        Request request = buildRequest();
        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.i("internet", result);
            apiResult = result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean result){
        listener.onRequestComplete(this.url, this.apiResult);
    }
}
