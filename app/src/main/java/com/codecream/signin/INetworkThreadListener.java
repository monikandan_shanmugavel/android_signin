package com.codecream.signin;

public interface INetworkThreadListener {
    public void onRequestComplete(String url, String resultStr);
}
